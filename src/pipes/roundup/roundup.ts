import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the RoundupPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'roundup',
})
export class RoundupPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
    transform(value: number): number {
      return Math.ceil(value);
  }
}
