import { NgModule } from '@angular/core';
import { RoundupPipe } from './roundup/roundup';
@NgModule({
	declarations: [RoundupPipe],
	imports: [],
	exports: [RoundupPipe]
})
export class PipesModule {}
