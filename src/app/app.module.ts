//標準
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//ページ
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ListPage } from '../pages/list/list';
import { NewaccountPage } from '../pages/newaccount/newaccount';
import { TeacherloginPage } from "../pages/teacherlogin/teacherlogin";
import { ChoicedeparPage } from "../pages/choicedepar/choicedepar";
import { ChoiceyearPage } from "../pages/choiceyear/choiceyear";
import { StudentlistPage } from "../pages/studentlist/studentlist";
import { AttendPage } from '../pages/attend/attend';
import { PassmailPage } from '../pages/passmail/passmail';
import { TabsPage } from '../pages/tabs/tabs'
import { LessonPage } from "../pages/lesson/lesson";
import { AnalyticsPage } from "../pages/analytics/analytics";
import { RecordsPage } from "../pages/records/records"
import { LesssonrecPage } from "../pages/lesssonrec/lesssonrec"
import { LogoutPage } from '../pages/logout/logout';
import { PastyearPage } from '../pages/pastyear/pastyear';
import { PaststudentlistPage } from '../pages/paststudentlist/paststudentlist';
import { PastattendPage } from '../pages/pastattend/pastattend';
import { TablePage } from '../pages/table/table';

//編集系
import { LessoneditPage } from "../pages/lessonedit/lessonedit";
import { DeperaddPage } from "../pages/deperadd/deperadd"
import { DepereditPage } from "../pages/deperedit/deperedit"
import { RecordeditPage } from "../pages/recordedit/recordedit"

//サービス！サービス！
import { User } from "../model/user";

// angularfire2
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestore } from '@angular/fire/firestore';

// environment
import { environment } from '../environment';
import { DatePipe } from '@angular/common';

//モジュール
import { IBeacon} from '@ionic-native/ibeacon';
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { BLE } from "@ionic-native/ble";
import { YearMonth } from '../model/datetime';
import { ChartsModule } from 'ng2-charts'
//import { Chart } from '../../node_modules/chart.js/src/charts/Chart.Bar'

//カスタムパイプ
import { RoundupPipe } from '../pipes/roundup/roundup';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ListPage,
    NewaccountPage,
    TeacherloginPage,
    ChoicedeparPage,
    ChoiceyearPage,
    StudentlistPage,
    AttendPage,
    PassmailPage,
    TabsPage,
    LessonPage,
    AnalyticsPage,
    RecordsPage,
    DeperaddPage,
    DepereditPage,
    RecordeditPage,
    LessoneditPage,
    LesssonrecPage,
    RoundupPipe,
    LogoutPage,
    PastyearPage,
    PaststudentlistPage,
    PastattendPage,
    TablePage
  ],
  imports: [
    BrowserModule,
    IonicModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicModule,
    ChartsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ListPage,
    NewaccountPage,
    TeacherloginPage,
    ChoicedeparPage,
    ChoiceyearPage,
    StudentlistPage,
    AttendPage,
    PassmailPage,
    TabsPage,
    LessonPage,
    AnalyticsPage,
    RecordsPage,
    DeperaddPage,
    DepereditPage,
    RecordeditPage,
    LessoneditPage,
    LesssonrecPage,
    LogoutPage,
    PastyearPage,
    PaststudentlistPage,
    PastattendPage,
    TablePage
  ],
  providers: [AngularFirestore,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    IBeacon,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatePipe,
    BLE,
    LocationAccuracy,
    User,
    YearMonth,
  ]
})
export class AppModule {}
