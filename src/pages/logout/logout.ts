import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController, AlertController } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from "angularfire2/database";

import { User } from '../../model/user';

import { LoginPage } from '../login/login';
/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(public navCtrl: NavController,
    private db:AngularFireDatabase,
    private firebaseAuth:AngularFireAuth,
    private toast: ToastController,
    private alertCtrl: AlertController,
    public user :User
    ) {
  }

  //ログアウト
  public logout() {
    this.firebaseAuth
      .auth
      .signOut()
      .then(_ => {
        this.toast.create({
          message: `logout`,
          duration: 3000
        }).present();

        this.navCtrl.parent.parent.setRoot(LoginPage);
        
      })
      .catch(e => {
        console.log(e);
        this.toast.create({
          message: `Can not logout`,
          duration: 3000
        }).present();
      });
  }
  
  //退会
  public Unsubscribe(){
    const confirm = this.alertCtrl.create({
      title: '退会しますか？',
      message: '退会すると今のアカウントが使用できなくなります。よろしいですか？',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Unsubscribe',
          handler: () => {
            this.firebaseAuth
            .auth
            .currentUser
            .delete()
            .then(state => {
              this.toast.create({
                message: `Unsubscribe`,
                duration: 3000
              }).present();
        
              console.log('deleted!');
                
              this.navCtrl.parent.parent.setRoot(LoginPage);
                
            })
            .catch(e => {
              console.error(e);
              this.toast.create({
                message: `Can not Unsubscribe`,
                duration: 3000
              }).present();
            });        
          }
        }
      ]
    });
    confirm.present();

  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
  }

}
