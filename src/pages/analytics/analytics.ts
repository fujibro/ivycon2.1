import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { User } from '../../model/user';
import { YearMonth } from '../../model/datetime';
import { DatePipe } from '@angular/common';

/**
 * Generated class for the AnalyticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-analytics',
  templateUrl: 'analytics.html',
})
export class AnalyticsPage {

  //firebaseのデータの入れ子
  items: Observable<any[]>;
  UID: string;

  data = new Date;
  i
  //年月日
  ymd: string;
  
  //追加する状態
  addrec: string;

  //月のデータ
  jan //1月
  feb //2月
  mar //3月
  apr //4月
  may //5月
  jun //6月
  jul //7月
  aug //8月
  sep //9月
  oct //10月
  nov //11月
  dec //12月

    //出席率
    Attend: any[] = new Array;

    //状態
    dayst: any[] = new Array;

    //key
    keys:any[] = new Array;

    //総日数
    sumday;

    dd: any;

    barChart: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase, private datePipe: DatePipe,private user :User,public yearmonth:YearMonth) {

  //サービスから受け取る
    this.UID = this.user.uid;

    //初期化
    this.yearmonth.ym = null;
    this.yearmonth.ym = this.datePipe.transform(this.data, 'y') + '-' + this.datePipe.transform(this.data, 'MM');
    
      //データベースからデータ取得(keyを含める)
      this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym,'y'))
      .snapshotChanges()
      .pipe(map(items => {
       return items.map(a => {
        
        const key = a.payload.key;
        const data = a.payload.val();
        this.dayst.push({key,data}) 
        console.dir(this.dayst)

      });
    })).subscribe();
  }

  //日付の変更
  public YMChange(){
    this.dayst = new Array;
    //状態
  //データベースからデータ取得(keyを含める)
    this.db.list('Record/' + this.UID + 
    '/' +  this.datePipe.transform(this.yearmonth.ym,'y'))
    .snapshotChanges()
    .pipe(map(items => {
      return items.map(a => {
        
      const key = a.payload.key;
      const data = a.payload.val();

      this.dayst.push({key,data}) 
      console.dir(this.dayst)
      });
    })).subscribe();
  }

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend:{
      display:true,
      position: "bottom"
    },
    scales: {
      yAxes: [{
      ticks: {
      beginAtZero: true
      }
      }],
      xAxes: [{
      ticks: {
      autoSkip: false
      }
      }]
      }
  };

  //ラベル
  public barChartLabels:string[] = [ '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月','1月', '2月', '3月'];

  //グラフの形
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  
  //データ
  public barChartData:any[] = [
    {data: [this.apr,this.may,this.jun,this.jul,this.aug,this.sep,this.oct,this.nov,this.dec,this.jan,this.feb,this.mar], label: '出席日数'}
  ];
  
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }

  public randomize():void {
    // Only Change 3 values
    let data = [this.apr,this.may,this.jun,this.jul,this.aug,this.sep,this.oct,this.nov,this.dec,this.jan,this.feb,this.mar];

    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;

    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }


  private judge(i){
  // 月ごとにデータを入れる
  switch(i){
    case "01":{
    this.jan = this.sumday;
    break;
    }
    case "02":{
    this.feb = this.sumday;
    break;
    }
    case "03":{
    this.mar = this.sumday;
    break;
    }
    case "04":{
    this.apr = this.sumday;
    break;
    }
    case "05":{
    this.may = this.sumday;
    break;  
    }
    case "06":{
    this.jun = this.sumday;
    break;
    }
    case "07":{
    this.jul = this.sumday;
    break; 
    }
    case "08":{
    this.aug = this.sumday;
    break;
    }
    case "09":{
    this.sep = this.sumday;
    break;
    }
    case "10":{
    this.oct = this.sumday;
    }
    case "11":{
    this.nov = this.sumday;
    break;
    }
    case "12":{
    this.dec = this.sumday;
    break;
    }
  }
}

  private filter(){

    //一ヶ月ごとに見る
    for(var i=0; i<=11; i++){  
      this.sumday = 0;

        for(var day = 1; day < 31; day++){

          ////////datepipeはゴミ
          this.dd = day;
          if(day < 10){
            this.dd = '0' + day;
          }
      //月があれば
      if(this.dayst[i] != undefined ){
          if(this.dayst[i].data !=undefined){
            if(this.dayst[i].data[this.dd] != undefined){
              if(this.dayst[i].data[this.dd].Status == "出席"){
                  this.sumday++
                  //if(this.dayst[i].key != undefined){
                  this.judge(this.dayst[i].key)
                    }
                  }
                }
              }
            }
          }
          this.randomize()
        }

        //グラフ表示用変数の初期化
        private Reset(){
          
          this.jan = 0;
          this.feb = 0;
          this.mar = 0;
          this.apr = 0;
          this.may = 0;
          this.jun = 0;
          this.jul = 0;
          this.aug = 0;
          this.sep = 0;
          this.oct = 0;
          this.nov = 0;
          this.dec = 0;
          
        }

        private Output(){
          //表示する前に初期化
          this.Reset();
          //表示
          this.filter();
        }

  }
