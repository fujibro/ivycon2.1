import { Component } from '@angular/core';
import { NavParams, ToastController } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

import { YearMonth } from '../../model/datetime'

/**
 * Generated class for the RecordeditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-recordedit',
  templateUrl: 'recordedit.html',
})
export class RecordeditPage {

  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //UID
  UID: string;

  data = new Date;

  //年月日
  ymd: string;
  
  //追加する状態
  addrec: string;
  
  constructor(public navParams: NavParams,
    private db: AngularFireDatabase,
    public yearmonth:YearMonth,
    private toast: ToastController,
    private datePipe: DatePipe) {
      //前ページから値受け取り
      this.UID = navParams.get("UID");

      this.ymd = this.datePipe.transform(this.data, 'y') + 
      '-' + this.datePipe.transform(this.data, 'MM') + 
      '-' + this.datePipe.transform(this.data, 'dd');
      
      //4月以降
      if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
        //データベースからデータ取得(keyを含める)
        this.items = this.db.list('Record/' + this.UID + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'y') + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
        .snapshotChanges()
        .pipe(map(items => {
          return items.map(a => {
            const data = a.payload.val();
            const key = a.payload.key;
            return {key, ...data};           // or {key, ...data} in case data is Obj
          });
        }));
      }else{ //4月以前
        //データベースからデータ取得(keyを含める)
        this.items = this.db.list('Record/' + this.UID + 
        '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
        .snapshotChanges()
        .pipe(map(items => {
          return items.map(a => {
            const data = a.payload.val();
            const key = a.payload.key;
            return {key, ...data};           // or {key, ...data} in case data is Obj
          });
        }));
      }
  }

  //出欠の更新
  public onChange(event,Record){
    //4月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      if(event == "削除"){
        this.db.object('Record/' + this.UID + 
        '/' + this.datePipe.transform(this.yearmonth.ym, 'y') + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM') + 
        '/' + Record)
        .remove();
        this.db.object('LessonRecord/' + this.UID + 
        '/' +  + this.datePipe.transform(this.yearmonth.ym, 'y') + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM') +
        '/' + Record)
        .remove(); 
      }else{
        this.db.object('Record/' + this.UID + 
        '/' + this.datePipe.transform(this.yearmonth.ym, 'y') + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM') + 
        '/' + Record)
        .update({Status: event});
      }
    }else{ //4月以前
      if(event == "削除"){
        this.db.object('Record/' + this.UID + 
        '/' + String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM') + 
        '/' + Record)
        .remove();
        this.db.object('LessonRecord/' + this.UID + 
        '/' +  + this.datePipe.transform(this.yearmonth.ym, 'y') + 
        '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM') +
        '/' + Record)
        .remove();
      }else{
        this.db.object('Record/' + this.UID + 
        '/' + String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
        '/' + this.datePipe.transform(this.yearmonth.ym, 'MM') + 
        '/' + Record)
        .update({Status: event});
      }
    }
    this.toast.create({
      message: `出欠の更新`,
      duration: 3000
    }).present();

  }

  //日付の変更
  public YMChange(){
    //4月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y') + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));
    }else{ //4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));
    }
  }

  //出欠追加
  public addrecord(){
    //4月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      this.db.object('Record/' + this.UID + 
      '/' + this.datePipe.transform(this.ymd, 'y') +
      '/' + this.datePipe.transform(this.ymd, 'MM') + 
      '/' + this.datePipe.transform(this.ymd, 'dd')).set({Status: this.addrec});
  
      if(this.addrec == "欠席"){
        //欠席状態の追加
        this.db.object('LessonRecord/' + this.UID + 
        '/' + this.datePipe.transform(this.ymd, 'y') + 
        '/' + this.datePipe.transform(this.ymd, 'MM') +
        '/' + this.datePipe.transform(this.ymd, 'dd'))
        .set({
          "1限目": "欠席",
          "2限目": "欠席",
          "3限目": "欠席",
          "4限目": "欠席",
          "5限目": "欠席",
          "6限目": "欠席",
          "7限目": "欠席"
        });
      }
      if(this.addrec == "公欠"){
        //欠席状態の追加
        this.db.object('LessonRecord/' + this.UID + 
        '/' + this.datePipe.transform(this.ymd, 'y') + 
        '/' + this.datePipe.transform(this.ymd, 'MM') +
        '/' + this.datePipe.transform(this.ymd, 'dd'))
        .set({
          "1限目": "公欠",
          "2限目": "公欠",
          "3限目": "公欠",
          "4限目": "公欠",
          "5限目": "公欠",
          "6限目": "公欠",
          "7限目": "公欠"
        });
      }  
    }else{ //4月以前
      this.db.object('Record/' + this.UID + 
      '/' + String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) +
      '/' + this.datePipe.transform(this.ymd, 'MM') + 
      '/' + this.datePipe.transform(this.ymd, 'dd')).set({Status: this.addrec});
  
      if(this.addrec == "欠席"){
        //欠席状態の追加
        this.db.object('LessonRecord/' + this.UID + 
        '/' + this.datePipe.transform(this.ymd, 'y') + 
        '/' + this.datePipe.transform(this.ymd, 'MM') +
        '/' + this.datePipe.transform(this.ymd, 'dd'))
        .set({
          "1限目": "欠席",
          "2限目": "欠席",
          "3限目": "欠席",
          "4限目": "欠席",
          "5限目": "欠席",
          "6限目": "欠席",
          "7限目": "欠席"
        });
      }
      if(this.addrec == "公欠"){
        //欠席状態の追加
        this.db.object('LessonRecord/' + this.UID + 
        '/' + this.datePipe.transform(this.ymd, 'y') + 
        '/' + this.datePipe.transform(this.ymd, 'MM') +
        '/' + this.datePipe.transform(this.ymd, 'dd'))
        .set({
          "1限目": "公欠",
          "2限目": "公欠",
          "3限目": "公欠",
          "4限目": "公欠",
          "5限目": "公欠",
          "6限目": "公欠",
          "7限目": "公欠"
        });
      }  
    }
    
    this.toast.create({
      message: `出欠の追加`,
      duration: 3000
    }).present();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecordeditPage');
    
  }

}
