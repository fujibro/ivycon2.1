import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import {NgModule} from '@angular/core';
import { User } from '../../model/user';
import { TabsPage } from "../tabs/tabs";
import { NewaccountPage } from '../newaccount/newaccount';
import { TeacherloginPage } from '../teacherlogin/teacherlogin';
import { PassmailPage } from '../passmail/passmail';

@NgModule({})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage{

  constructor(
    public navCtrl: NavController,
    private afAuth: AngularFireAuth,
    private toast: ToastController,
    public user :User
  ) {}

  // Login
  async login(user: User) {
    this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then(res => {
        // Login succeed.
        if (res.user.email && res.user.uid) {
            this.user.uid =  res.user.uid;
          // Go to HomePage.
          //this.navCtrl.setRoot(HomePage,{uid:res.user.uid});
          this.navCtrl.push(TabsPage,{uid:res.user.uid});
          // display the success message.
          this.toast.create({
            message: `Welcome to Ivycon 2.0!`,
            duration: 3000
          }).present();
        } else {
          this.toast.create({
            message: `Could not find authentication details.`,
            duration: 3000
          }).present();
        }
      })
      .catch(err => {
        // Login failed.
        this.toast.create({
          message: `Login Id and Password do not match.`,
          duration: 3000
        }).present();
      });
  }

  public newaccountsend(){
    this.navCtrl.setRoot(NewaccountPage);
  }

  public tcherlogin(){
    this.navCtrl.setRoot(TeacherloginPage);

  }
  
  public passsend(){
    this.navCtrl.push(PassmailPage);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
