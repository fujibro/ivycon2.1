import { Component } from '@angular/core';
import { ToastController, AlertController } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

/**
 * Generated class for the DepereditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-deperedit',
  templateUrl: 'deperedit.html',
})
export class DepereditPage {

  //firebaseのデータ
  items: Observable<any[]>;

  //変更名
  rename: string;

  //学科
  Deper: string;

  constructor(private db:AngularFireDatabase,
    private toast: ToastController,
    private alertCtrl: AlertController) {
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Deper/')
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key,  data};           // or {key, ...data} in case data is Obj
        });
      }));
  }

  //学科の編集
  public editdeper(){
    //学科の変更
    this.db.object('Deper/'+this.Deper).set(this.rename);
    this.toast.create({
      message: `学科の編集完了`,
      duration: 3000
    }).present();

  }

  //学科の削除
  public removedeper(){
    const confirm = this.alertCtrl.create({
      title: '選択した学科を削除しますか？',
      message: '学科を削除するとその学科の学生の情報、学校への登校や授業の出欠に関するデータが全て削除されます！',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Delete',
          handler: () => {
            //一年生
            this.db.list('Student/'+this.Deper+'/1')
            .snapshotChanges()
            .pipe(map(items => {
              return items.map(a => {
                const key = a.payload.key;
                //生徒の削除
                this.db.object('Student/'+this.Deper+'/1/'+ key).remove();
                //出欠の削除
                this.db.object('Record/'+key).remove();
                //授業ごとの出欠の削除
                this.db.object('LessonRecord/'+key).remove();
              });
            })).subscribe();

            //二年生
            this.db.list('Student/'+this.Deper+'/2')
            .snapshotChanges()
            .pipe(map(items => {
              return items.map(a => {
                const key = a.payload.key;
                //生徒の削除
                this.db.object('Student/'+this.Deper+'/2/'+ key).remove();
                //出欠の削除
                this.db.object('Record/'+key).remove();
                //授業ごとの出欠の削除
                this.db.object('LessonRecord/'+key).remove();
              });
            })).subscribe();

            //三年生
            this.db.list('Student/'+this.Deper+'/3')
            .snapshotChanges()
            .pipe(map(items => {
              return items.map(a => {
                const key = a.payload.key;
                //生徒の削除
                this.db.object('Student/'+this.Deper+'/3/'+ key).remove();
                //出欠の削除
                this.db.object('Record/'+key).remove();
                //授業ごとの出欠の削除
                this.db.object('LessonRecord/'+key).remove();
              });
            })).subscribe();
            //学科の削除
            this.db.object('Deper/'+this.Deper).remove();
            //学年の削除
            this.db.object('YearDeper/'+this.Deper).remove();
            this.toast.create({
              message: `学科削除`,
              duration: 3000
            }).present();
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DepereditPage');
  }

}
