import {  Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { User } from '../../model/user';
import { YearMonth } from '../../model/datetime';
import { LesssonrecPage } from '../../pages/lesssonrec/lesssonrec'

import { DatePipe } from '@angular/common';

/**
 * Generated class for the AttendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-records',
  templateUrl: 'records.html',
})
export class RecordsPage {


  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //受け取る変数
  //UID
  //UID
  UID: string;

  data = new Date;

  //年月日
  ymd: string;
  
  //追加する状態
  addrec: string;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase, private datePipe: DatePipe,private user :User,public yearmonth:YearMonth) {
    //前ページから値受け取り
    this.UID = this.user.uid
   // this.Name = navParams.get("Name");

    //初期化
    this.yearmonth.ym = null;
    this.yearmonth.ym = this.datePipe.transform(this.data, 'y') + '-' + this.datePipe.transform(this.data, 'MM');
    
    //４月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y') + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));
    }else{//4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));
    }
  }

  //年月の変更
  public YMChange(){
    //４月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y') + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));
    }else{//4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));
    }
  }
  
  //授業画面
  public itemSelected(day){
    this.navCtrl.push(LesssonrecPage,{
      //UID送る
      UID: this.UID,
      Day: day
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AttendPage');
  }

}
