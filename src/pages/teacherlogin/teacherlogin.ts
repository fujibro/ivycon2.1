import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../model/user';

import { ChoicedeparPage } from '../choicedepar/choicedepar';
import { LoginPage } from '../login/login';

/**
 * Generated class for the TeacherloginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-teacherlogin',
  templateUrl: 'teacherlogin.html',
})
export class TeacherloginPage {

  user = {} as User;

  constructor(
    public navCtrl: NavController,
    private afAuth: AngularFireAuth,
    private toast: ToastController,

  ) {
    this.user.email = "test@test.com";
    this.user.password = "testtest";
  }

  // Login
  async login(user: User) {
    this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then(res => {
        // Login succeed.
        if (res.user.email && res.user.uid) {
          if (res.user.uid == "yFc7ggUfOybB1HnctTgpmJU0FO72"){
            // Go to HomePage.
            this.navCtrl.setRoot(ChoicedeparPage);

            // display the success message.
            this.toast.create({
              message: `Welcome to Ivycon 2.0!`,
              duration: 3000
            }).present();
          } else {
            this.toast.create({
              message: `アカウントが違います`,
              duration: 3000
            }).present();
            }
        } else {
          this.toast.create({
            message: `アカウントが違います`,
            duration: 3000
          }).present();
        }
      })
      .catch(err => {
        // Login failed.
        this.toast.create({
          message: `パスワードまたはメールアドレスが違います`,
          duration: 3000
        }).present();
      });
  }

  //生徒側のログインページに移動  
  public studentlogin (){
    this.navCtrl.setRoot(LoginPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TeacherloginPage');
  }

}
