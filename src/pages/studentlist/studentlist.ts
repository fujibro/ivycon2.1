import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';

import { AttendPage } from '../attend/attend';
import { TablePage } from '../table/table';

import { map } from 'rxjs/operators';

import { DatePipe } from '@angular/common';

/**
 * Generated class for the StudentlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-studentlist',
  templateUrl: 'studentlist.html'
})
export class StudentlistPage {

  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //リスト構造
  dayArray: any[] = new Array;

  //受け取る変数
  //学科
  Deper: string;
  //学年
  Year: string;
  
  data = new Date();

  //年
  year = this.datePipe.transform(this.data, 'y');
  
  //月
  month = this.datePipe.transform(this.data, 'MM');

  //日
  day = this.datePipe.transform(this.data, 'dd');

  //for文用の変数
  cnt;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private db: AngularFireDatabase, private datePipe: DatePipe) {
    //前ページから値受け取り
    this.Deper = navParams.get("Deper");
    this.Year = navParams.get("Year");

    //データベースからデータ取得(keyを含める)
    this.items = this.db.list('Student/' + this.Deper + '/' + this.Year)
    .snapshotChanges()
    .pipe(map(getdata => {
      return getdata.map(a => {
        //リストの初期化
        if(this.dayArray.length != 0){
          this.dayArray = new Array;
          console.log("初期化");
        }
        //valueを取る
        const data = a.payload.val();
        //keyをとる
        const key = a.payload.key;
        //状態を見に行く
        //4月以降
        if(Number(this.month) > 3){
          this.db.list('Record/' + key + '/' + this.year + '/' + this.month + '/' + this.day)
          .valueChanges()
          .subscribe(item => {//よくわからんが順番に走る。止まるんじゃねえぞ...
            //中味がなかったら
            if(item[0] == undefined){
              //未定義かの確認
              if(this.dayArray.length != 0){
                //keyの比較
                for(this.cnt = 0;this.dayArray.length > this.cnt; this.cnt++){
                  if(this.dayArray[this.cnt].key == key){
                    this.dayArray[this.cnt] = ({key: key, Status: " "});
                    this.cnt = 0
                    break;
                  }
                }
                if(this.cnt == this.dayArray.length){
                  //空文字を入れる
                  this.dayArray.push({key: key, Status: " "});
                }
              }else{
              //空文字を入れる
              this.dayArray.push({key: key, Status: " "});
            }
            }else{
              //未定義かの確認
              if(this.dayArray.length != 0){
                //keyの比較
                for(this.cnt = 0;this.dayArray.length > this.cnt; this.cnt++){
                  if(this.dayArray[this.cnt].key == key){
                    //アイテムを入れる
                    this.dayArray[this.cnt] = ({key: key, Status: item[0]});
                    this.cnt = 0
                    break;
                  }
                }
                if(this.cnt == this.dayArray.length){
                  //アイテムを入れる
                  this.dayArray.push({key: key, Status: item[0]});
                }
              }else{
                //アイテムを入れる
                this.dayArray.push({key: key, Status: item[0]});
              }
            }
          });  
        }else{//４月以前
          this.db.list('Record/' + key + '/' + String(Number(this.year) - 1) + '/' + this.month + '/' + this.day)
          .valueChanges()
          .subscribe(item => {//よくわからんが順番に走る。止まるんじゃねえぞ...
            console.log("key:"+key);
            //中味がなかったら
            if(item[0] == undefined){
              //未定義かの確認
              if(this.dayArray.length != 0){
                console.log("定義済！の値はundefined");
                //keyの比較
                for(this.cnt = 0;this.dayArray.length > this.cnt; this.cnt++){
                  if(this.dayArray[this.cnt].key == key){
                    this.dayArray[this.cnt] = ({key: key, Status: " "});
                    this.cnt = 0
                    break;
                  }
                }
                if(this.cnt == this.dayArray.length){
                  //空文字を入れる
                  this.dayArray.push({key: key, Status: " "});
                }
              }else{
              //空文字を入れる
              this.dayArray.push({key: key, Status: " "});
            }
            }else{
              //未定義かの確認
              if(this.dayArray.length != 0){
                //keyの比較
                for(this.cnt = 0;this.dayArray.length > this.cnt; this.cnt++){
                  if(this.dayArray[this.cnt].key == key){
                    //アイテムを入れる
                    this.dayArray[this.cnt] = ({key: key, Status: item[0]});
                    this.cnt = 0
                    break;
                  }
                }
                if(this.cnt == this.dayArray.length){
                  //アイテムを入れる
                  this.dayArray.push({key: key, Status: item[0]});
                }
              }else{
                //アイテムを入れる
                this.dayArray.push({key: key, Status: item[0]});
              }
            }
          });  
        }
        //keyとvalue返す
        return {key, ...data};
      });
    }));
  }

  //クリックイベント
  public itemSelected(uid, name){

   this.navCtrl.push(AttendPage,{
      //選択したUIDと名前を渡す
      UID: uid,
      Name: name
    });

  }

  //表
  public send(){
    this.navCtrl.push(TablePage,{
      //選択したUIDと名前を渡す
      Deper: this.Deper,
      Year: this.Year,
      DName: this.navParams.get("DName"),
      year: this.navParams.get("year")
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StudentlistPage');
  }

}
