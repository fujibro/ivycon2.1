import { Component ,NgModule, OnInit, OnDestroy} from '@angular/core';
import { NavController,Platform} from 'ionic-angular';
import { IBeacon} from '@ionic-native/ibeacon';
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { BLE } from "@ionic-native/ble";
import { User } from "../../model/user";
import { AngularFireDatabase} from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import { map } from 'rxjs/operators';

@NgModule({
  providers: [
]})

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage implements OnInit, OnDestroy{

  delegate :any;
  region :any; //範囲
  log; //ログ
  myDate;
  text;
  year; //年
  month //月
  day //日
  time 
  data//時間
  ibc
  date

  //現在時刻
  now: Observable<Date>;
  intervalList = [];

  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //リスト構造
  itemlist: any[] = new Array;

  
  public ngOnInit() {
    this.now = new Observable((observer) => {
      this.intervalList.push(setInterval(() => {
        observer.next(new Date());
      }, 1000));
    });

    this.text = "登校登録を行なう"
  }

  ngOnDestroy() {
    if (this.intervalList) {
      this.intervalList.forEach((interval) => {
        clearInterval(interval);
      });
    }
  }

  constructor(
    public navCtrl: NavController,
    private ibeacon : IBeacon,
    private db: AngularFireDatabase,
    private datePipe: DatePipe,
    private ble : BLE,
    private locationAccuracy: LocationAccuracy,
    private user :User,
    private platform:Platform
  )
  {

    this.text = "登校登録を行なう"
    
    this.GpsFun();
    this.BleFun();

    const data = new Date();
    const formattedDate = this.datePipe.transform(data, 'y/MM/dd (EE) HH:mm');

    this.data = new Date();
    //フォーマット
    this.year = this.datePipe.transform(this.data, 'y');
    this.month = this.datePipe.transform(this.data, 'MM');
    this.day = this.datePipe.transform(this.data, 'dd');

  //今日の出席情報を表示
  //データベースからデータ取得(keyを含める)

  if(Number(this.datePipe.transform(this.month.ym, 'MM')) < 4){
    this.year = String(Number(this.year) - 1 )
  }

  this.items = this.db.list('Record/'+this.user.uid+'/'+this.year+ '/'+this.month+'/'+this.day)
  .snapshotChanges()
  .pipe(map(items => {
    return items.map(a => {
      const data = a.payload.val();
      const key = a.payload.key;
      //this.date = data
      console.log("test",data)
      return {data};           // or {key, ...data} in case data is Obj
      //return {data}; 
    });
  }));



}


public GpsFun(){
  this.platform.ready().then(() => {
    //GPSをonにさせる
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => console.log('Request successful'),
          error => console.log('Error requesting location permissions', error)
        );
      }
    });
  });
}


  public BleFun(){
    this.platform.ready().then(() => {
    //bluetoothをonにさせる
    this.ble.isEnabled().then(() => {
      console.log("bluetooth is enabled all G");
    }, () => {
      console.log("bluetooth is not enabled trying to enable it");
          this.ble.enable().then(() => {
              console.log("bluetooth got enabled hurray");
                }, () => {
                  console.log("user did not enabled");
          })
      });
    });
  }

  startScan(){
    
    console.log("initScanner!");

        //ivyの敷地内に入ったら 
        //現在時刻取る
        this.data = new Date();
        //フォーマット
        
        this.year = this.datePipe.transform(this.data, 'y');
        
        this.month = this.datePipe.transform(this.data, 'MM');
        //this.month = 1
        this.day = this.datePipe.transform(this.data, 'dd');
      
        this.time = this.datePipe.transform(this.data, 'HH:mm');

        //今日の授業用
        this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).set({"1限目": "欠席" , "2限目": "欠席" ,"3限目": "欠席","4限目": "欠席","5限目": "欠席","6限目": "欠席","7限目": "欠席"});

        //ビーコンを見つけられなければ欠席に
        //4月より大きければ去年のデータに
        if(Number(this.datePipe.transform(this.month.ym, 'MM')) < 4){
          this.year = String(Number(this.year) - 1 )
        }

        this.db.object('Record/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).set({Status: "欠席", Time: this.time});

    this.text = "ビーコンを検索中..."
    this.ibeacon.requestAlwaysAuthorization()
    this.ibeacon.requestWhenInUseAuthorization()


    let delegate = this.ibeacon.Delegate();
    
    delegate.didRangeBeaconsInRegion()
    .subscribe(
      //data => console.log('didRangeBeaconsInRegion:', data),
      error => console.error()
      );

    delegate.didStartMonitoringForRegion().subscribe(
      data=>{
        console.log('didStartMonitoringForRegion:',data);
      //error=>console.log(error)
      //this.ibeacon.requestStateForRegion(BeaconRegion);

      //iosはBeaconの領域内に入っている場合、locationManager:didEnterRegion: が呼ばれません。
      //this.ibc = this.ibeacon.requestStateForRegion(BeaconRegion)
      this.ibeacon.requestStateForRegion(BeaconRegion)
      //設定した値と同じなら
      if (this.ibc = BeaconRegion){
        console.log('ininin',data);
          delegate.didEnterRegion();
      }
      });


    delegate.didDetermineStateForRegion().subscribe(
      data=>console.log('didStartMonitoringForRegion:',data),
      error=>console.log(error)
      );

 
      //範囲に侵入
    delegate.didEnterRegion()
    .subscribe(
      data => {
      console.log('didEnterRegion: ', data);
      this.text = "ビーコンを発見"

      //8:50以内
      if(this.time < "08:50"){
        this.db.object('Record/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).set({Status: "出席", Time: this.time});

      }else{
        this.db.object('Record/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).set({Status: "遅刻", Time: this.time});
      }

      this.text = "登校登録完了！"
      this.ibeacon.stopMonitoringForRegion(BeaconRegion);
  });

    let BeaconRegion = this.ibeacon.BeaconRegion('test.beacon','48534442-4C45-4144-80C0-180000000000');

    //モニターの開始
    this.ibeacon.startMonitoringForRegion(BeaconRegion).then(
      () => console.log('Native layer received the request to monitoring:',JSON.stringify(this.region)),
      error => console.log('Native layer failed to begin monitoring: ', error))


    this.ibeacon.startRangingBeaconsInRegion(BeaconRegion).then(()=> {
      console.log('started ranging in beacon');
      //this.text = "ビーコンの中にいます";
    })

    this.ibeacon.requestStateForRegion(BeaconRegion).then(
      () => console.log('requestStateForRegion:',JSON.stringify(this.region)),
      error => console.log('requestStateForRegion: ', error))

    //範囲から退出
    //ボタンを押してenterしたらはいらない
    //exitした状態でボタンを押すことで初めて入る
    delegate.didExitRegion().subscribe(data => {
      console.log('didExitRegion: ', data);
      //this.text="ビーコンの範囲内から出ました";
    });
  };
}
