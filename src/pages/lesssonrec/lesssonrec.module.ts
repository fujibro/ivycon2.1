import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LesssonrecPage } from './lesssonrec';

@NgModule({
  declarations: [
    LesssonrecPage,
  ],
  imports: [
    IonicPageModule.forChild(LesssonrecPage),
  ],
})
export class LesssonrecPageModule {}
