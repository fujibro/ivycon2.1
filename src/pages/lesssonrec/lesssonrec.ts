import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

import { YearMonth } from '../../model/datetime'
/**
 * Generated class for the LesssonrecPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-lesssonrec',
  templateUrl: 'lesssonrec.html',
})
export class LesssonrecPage {


  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //受け取る変数
  //UID
  UID: string;

  //日
  day: string;

  //追加出欠状態
  addles: string;

  //何時間目か
  lesrec: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase,public yearmonth:YearMonth,
    private datePipe:DatePipe) {

      //前ページから値受け取り
      this.UID = navParams.get("UID");
      this.day = navParams.get("Day");

      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('LessonRecord/' + this.UID + 
      '/' + + this.datePipe.transform(this.yearmonth.ym, 'y') + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM') + 
      '/' + this.day)
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, data};           // or {key, ...data} in case data is Obj
        });
      }));
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LesssonrecPage');
  }

}
