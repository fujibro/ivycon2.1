import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { StudentlistPage } from '../studentlist/studentlist';
import { PastyearPage } from '../pastyear/pastyear';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

/**
 * Generated class for the ChoiceyearPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-choiceyear',
  templateUrl: 'choiceyear.html',
})
export class ChoiceyearPage {

  //受け取る変数
  //学科
  Deper: string;

  //学年
  yitems: Observable<any[]>;

  data = new Date;

  year = this.datePipe.transform(this.data, 'y');

  month = this.datePipe.transform(this.data, 'MM');

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase,
    private datePipe: DatePipe) {
    //前ページから値受け取り
    this.Deper = navParams.get("Deper");

    //データベースからデータ取得(keyを含める)
    this.yitems = this.db.list('YearDeper/' + this.Deper)
    .snapshotChanges()
    .pipe(map(items => {
      return items.map(a => {
        const key = a.payload.key;
        return {key};           // or {key, ...data} in case data is Obj
      });
    }));
    
  }

  //学年選択
  public itemSelected(item){
    //４月以降
    if(Number(this.month) > 3){
      switch(item){
        case "1": 
          item = this.year;
          break;
        case "2": 
          item = String(Number(this.year) - 1);
          break;
        case "3": 
          item = String(Number(this.year) - 2);
          break;
      }  
    }else{//４月以前
      switch(item){
        case "1": 
          item = String(Number(this.year) - 1);
          break;
        case "2": 
          item = String(Number(this.year) - 2);
          break;
        case "3": 
          item = String(Number(this.year) - 3);
          break;
      }  
    }
    //保存期間の過ぎた生徒の削除
    this.yitems.subscribe(a => {
      //４月以降
      if(Number(this.month) > 3){
        this.db.list('Student/' + this.Deper + '/' + String(Number(this.year) - a.length - 3))
        .snapshotChanges()
        .pipe(map(items => {
          return items.map(a => {
            const key = a.payload.key;
            this.db.object('Record/' + key).remove();
            this.db.object('LessonRecord/' + key).remove();
          });
        })).subscribe(_ => {
          this.db.object('Student/' + this.Deper + '/' + String(Number(this.year) - a.length - 3)).remove();
        });
      }else{//４月以前
        this.db.list('Student/' + this.Deper + '/' + String(Number(this.year) - a.length - 4))
        .snapshotChanges()
        .pipe(map(items => {
          return items.map(a => {
            const key = a.payload.key;
            this.db.object('Record/' + key).remove();
            this.db.object('LessonRecord/' + key).remove();
          });
        })).subscribe(_ => {
          this.db.object('Student/' + this.Deper + '/' + String(Number(this.year) - a.length - 4)).remove();
        });
      }
    });

    this.navCtrl.push(StudentlistPage,{
      //渡すデータ
      //学科
      Deper: this.Deper,
      DName: String(this.navParams.get("DName")),
      //学年
      Year: item,
      year: Number(this.year) - item
    });
  }

  //過去生徒
  public pastyearsend(){
    this.yitems.subscribe(a => {
      this.navCtrl.push(PastyearPage,{
        //渡すデータ
        //学科
        Deper: this.Deper,
        //学年
        Year: a.length
      });
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoiceyearPage');
  }

}
