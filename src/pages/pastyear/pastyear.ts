import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DatePipe } from '@angular/common';

import { PaststudentlistPage } from '../paststudentlist/paststudentlist';
/**
 * Generated class for the PastyearPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pastyear',
  templateUrl: 'pastyear.html',
})
export class PastyearPage {

  //過去の学年
  years = new Array;

  data = new Date;

  year = this.datePipe.transform(this.data, 'y');

  month = this.datePipe.transform(this.data, 'MM');

  //学科
  Deper;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,  
    private datePipe: DatePipe) {
      //前ページから値受け取り
      this.Deper = navParams.get("Deper");

      //４月以降
      if(Number(this.month) > 3){
        this.years.push(String(Number(this.year) - Number(navParams.get("Year")) - 3));
        this.years.push(String(Number(this.year) - Number(navParams.get("Year")) - 2));
        this.years.push(String(Number(this.year) - Number(navParams.get("Year")) - 1));
      }else{//４月以前
        this.years.push(String(Number(this.year) - Number(navParams.get("Year")) - 4));
        this.years.push(String(Number(this.year) - Number(navParams.get("Year")) - 3));
        this.years.push(String(Number(this.year) - Number(navParams.get("Year")) - 2));
      }

  }

  public paststudent(year){
    console.log(year);
    this.navCtrl.push(PaststudentlistPage,{
      //渡すデータ
      //学科
      Deper: this.Deper,
      //学年
      Year: year
    });

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PastyearPage');
  }

}
