import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ChoiceyearPage } from '../choiceyear/choiceyear';
import { DeperaddPage } from '../deperadd/deperadd';
import { DepereditPage } from '../deperedit/deperedit';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';


/**
 * Generated class for the ChoicedeparPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-choicedepar',
  templateUrl: 'choicedepar.html',
})
export class ChoicedeparPage {

  //学科
  ditems: Observable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase) {
    //データベースからデータ取得(keyを含める)
    this.ditems = this.db.list('Deper/')
    .snapshotChanges()
    .pipe(map(items => {
      return items.map(a => {
        const data = a.payload.val();
        const key = a.payload.key;
        return {key,  data};           // or {key, ...data} in case data is Obj
      });
    }));
  }
  //リストの選択イベント
  public itemSelected(item, name){
    this.navCtrl.push(ChoiceyearPage,{
      //選択した学科を渡す
      Deper: item,
      DName: String(name)
    });
  }
  
  //学科追加
  public sendadd(){
    this.navCtrl.push(DeperaddPage);
  }

  //学科編集
  public sendedit(){
    this.navCtrl.push(DepereditPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoicedeparPage');
  }

}
