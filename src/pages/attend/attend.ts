import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { DatePipe } from '@angular/common';

import { RecordeditPage } from '../recordedit/recordedit';
import { LessoneditPage } from '../lessonedit/lessonedit';

import { YearMonth } from '../../model/datetime'

import 'rxjs/add/operator/first';

/**
 * Generated class for the AttendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-attend',
  templateUrl: 'attend.html',
})
export class AttendPage {

  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //受け取る変数
  //UID
  UID: string;
  //生徒の名前
  Name: string;

  data = new Date();

  //出席率
  Attend: any[] = new Array;

  //総日数
  sumday;

  //欠席
  ab;
  
  //遅刻早退
  late;

  dd: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private db: AngularFireDatabase, public datePipe: DatePipe,
    private yearmonth:YearMonth) {
    //前ページから値受け取り
    this.UID = navParams.get("UID");
    this.Name = navParams.get("Name");

    //初期化
    this.yearmonth.ym = null;
    this.yearmonth.ym = this.datePipe.transform(this.data, 'y') + '-' + this.datePipe.transform(this.data, 'MM');
    
    //４月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y') + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y'))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }else{//4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }
  }

  //出欠編集画面
  public sendedit(){
    this.navCtrl.push(RecordeditPage,{
      //UID、年、月を送る
      UID: this.UID
    });
  }
  
  //年月の変更
  public YMChange(){
    //４月以降
    if(Number(this.datePipe.transform(this.yearmonth.ym, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y') + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'y'))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }else{//4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.yearmonth.ym, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.yearmonth.ym, 'y')) - 1 ))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }
  }

  //授業画面
  public itemSelected(day){
    this.navCtrl.push(LessoneditPage,{
      //UID送る
      UID: this.UID,
      Day: day
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AttendPage');
  }

}
