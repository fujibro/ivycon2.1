import { Component ,NgModule, OnInit, OnDestroy} from '@angular/core';
import { NavController ,Platform} from 'ionic-angular';
import { IBeacon} from '@ionic-native/ibeacon';
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { BLE } from "@ionic-native/ble";
import { User } from "../../model/user";
import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import { map } from 'rxjs/operators';

@NgModule({
  providers: [
]})

@Component({
  selector: 'page-lesson',
  templateUrl: 'lesson.html'
})


export class LessonPage implements OnInit, OnDestroy{

  delegate :any;
  region :any; //範囲
  log; //ログ
  myDate;
  text;
  year; //年
  month //月
  day //日
  time 
  data//時間
  ibc

  //現在時刻
  now: Observable<Date>;
  intervalList = [];

  //firebaseのデータの入れ子
  items: Observable<any[]>;


  public ngOnInit() {
    this.now = new Observable((observer) => {
      this.intervalList.push(setInterval(() => {
        observer.next(new Date());
      }, 1000));
    });
  }

  ngOnDestroy() {
    if (this.intervalList) {
      this.intervalList.forEach((interval) => {
        clearInterval(interval);
      });
    }
  }

  constructor(
    public navCtrl: NavController,
    private ibeacon : IBeacon,
    private db: AngularFireDatabase,
    private datePipe: DatePipe,
    private ble : BLE,
    private locationAccuracy: LocationAccuracy,
    private user :User,
    private platform:Platform
  )
  {

    this.text = "出席登録を行なう"

    this.GpsFun();
    this.BleFun();

    const data = new Date();
    const formattedDate = this.datePipe.transform(data, 'y/MM/dd (EE) HH:mm');

    this.data = new Date();
    //フォーマット
    this.year = this.datePipe.transform(this.data, 'y');
    this.month = this.datePipe.transform(this.data, 'MM');
    this.day = this.datePipe.transform(this.data, 'dd');

  //データベースからデータ取得(keyを含める)
  this.items = this.db.list('LessonRecord/'+this.user.uid+'/'+this.year+ '/'+this.month+'/'+this.day)
  .snapshotChanges()
  .pipe(map(items => {
    return items.map(a => {
      const data = a.payload.val();
      const key = a.payload.key;
      console.log(key,data)
      return {key,data};           // or {key, ...data} in case data is Obj
      
      //return {data}; 
    });
  }));
    
  }

  public GpsFun(){
    this.platform.ready().then(() => {
      //GPSをonにさせる
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if(canRequest) {
          // the accuracy option will be ignored by iOS
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => console.log('Request successful'),
            error => console.log('Error requesting location permissions', error)
          );
        }
      });
    });
  }
  
  
    public BleFun(){
      this.platform.ready().then(() => {
      //bluetoothをonにさせる
      this.ble.isEnabled().then(() => {
        console.log("bluetooth is enabled all G");
      }, () => {
        console.log("bluetooth is not enabled trying to enable it");
            this.ble.enable().then(() => {
                console.log("bluetooth got enabled hurray");
                  }, () => {
                    console.log("user did not enabled");
            })
        });
      });
    }


  startScan(){
    
    console.log("initScanner!");

        //ivyの敷地内に入ったら 
        //現在時刻取る
        this.data = new Date();
        //フォーマット
        
        this.year = this.datePipe.transform(this.data, 'y');
        
        this.month = this.datePipe.transform(this.data, 'MM');
      
        this.day = this.datePipe.transform(this.data, 'dd');
      
        this.time = this.datePipe.transform(this.data, 'HH:mm');
        

    this.text = "ビーコンを検索中..."
    this.ibeacon.requestAlwaysAuthorization()
    this.ibeacon.requestWhenInUseAuthorization()


    let delegate = this.ibeacon.Delegate();
    
    delegate.didRangeBeaconsInRegion()
    .subscribe(
      //data => console.log('didRangeBeaconsInRegion:', data),
      error => console.error()
      );

      delegate.didStartMonitoringForRegion().subscribe(
        data=>{
          console.log('didStartMonitoringForRegion:',data);
        //error=>console.log(error)
        //this.ibeacon.requestStateForRegion(BeaconRegion);
        
        //iosはBeaconの領域内に入っている場合、locationManager:didEnterRegion: が呼ばれません。
        //this.ibc = this.ibeacon.requestStateForRegion(BeaconRegion)
        this.ibeacon.requestStateForRegion(BeaconRegion)
        //設定した値と同じなら
        if (this.ibc = BeaconRegion){
          console.log('ininin',data);
            delegate.didEnterRegion();
        }
        });

 
      //範囲に侵入
    delegate.didEnterRegion()
    .subscribe(
      data => {
      console.log('didEnterRegion: ', data);
      this.text = "ビーコンを発見"

        //1コマ目10分前
        if(this.time < "09:10" && this.time > "09:00"){ 
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"1限目": "出席"});
        }else if(this.time > "09:11" && this.time <"10:00"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"1限目": "遅刻"});
        }

        //2コマ目10分前
        if(this.time < "10:10" && this.time > "10:00"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"2限目": "出席"});
        }else if(this.time > "10:11" && this.time <"11:00"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"2限目": "遅刻"});
        }

        //3コマ目10分前
        if(this.time < "11:10" && this.time > "11:00"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"3限目": "出席"});
        }else if(this.time > "11:11" && this.time <"12:00"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"3限目": "遅刻"});
        }

        //4コマ目10分前
        if(this.time < "13:00" && this.time > "12:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"4限目": "出席"});
        }else if(this.time>"13:01" && this.time< "13:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"4限目": "遅刻"});
        }

        //5コマ目10分前
        if(this.time < "14:00" && this.time > "13:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"5限目": "出席"});
        }else if(this.time>"14:01" && this.time< "14:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"5限目": "遅刻"});
        }

        //6コマ目10分前
        if(this.time < "15:00" && this.time > "14:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"6限目": "出席"});
        }else if(this.time>"15:01" && this.time< "15:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"6限目": "遅刻"});
        }

        //7コマ目10分前
        if(this.time < "16:00" && this.time > "15:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"7限目": "出席"});
        }else if(this.time>"16:01" && this.time< "16:50"){
          this.db.object('LessonRecord/'+this.user.uid+'/'+ this.year + '/' + this.month + '/' + this.day).update({"7限目": "遅刻"});
        }        
        this.text = "出席登録完了！"

        this.ibeacon.stopMonitoringForRegion(BeaconRegion);

  });

    let BeaconRegion = this.ibeacon.BeaconRegion('test.beacon','48534442-4C45-4144-80C0-180000000000');

    //モニターの開始
    this.ibeacon.startMonitoringForRegion(BeaconRegion).then(
      () => console.log('Native layer received the request to monitoring:',JSON.stringify(this.region)),
      error => console.log('Native layer failed to begin monitoring: ', error))


    this.ibeacon.startRangingBeaconsInRegion(BeaconRegion).then(()=> {
      console.log('started ranging in beacon');
      //this.text = "ビーコンの中にいます"; 
    }
    )
    //範囲から退出
    //ボタンを押してenterしたらはいらない
    //exitした状態でボタンを押すことで初めて入る
    
    delegate.didExitRegion().subscribe(data => {
      console.log('didExitRegion: ', data);
      this.text="ビーコンの範囲内から出ました";
    });
  };
}
