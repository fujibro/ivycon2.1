import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the PassmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-passmail',
  templateUrl: 'passmail.html',
})
export class PassmailPage {

  //メールアドレス
  email: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth,
    private toast: ToastController) {
    
  }
  
  //パスワードの再設定
  public passsend(){
    this.afAuth.auth.sendPasswordResetEmail(this.email)
        .then(() =>  {
          this.toast.create({
            message: `メールを送りました`,
            duration: 3000
          }).present();  
        })           
        .catch((error) => {
          this.toast.create({
            message: `メールが送れませんでした`,
            duration: 3000
          }).present();  
        })
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad PassmailPage');
  }

}
