import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../model/user';
import { LoginPage } from '../login/login';

import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

/**
 * Generated class for the NewaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-newaccount',
  templateUrl: 'newaccount.html',
})
export class NewaccountPage {

  user = {} as User;

  //学科
  ditems: Observable<any[]>;
  Deper: string;

  //学年
  yitems: Observable<any[]>
  Year: String;

  //名前
  name: any;
  //学籍番号
  number: String;

  data = new Date;

  year = this.datePipe.transform(this.data, 'y');
  
  month = this.datePipe.transform(this.data, 'MM');

  day = this.datePipe.transform(this.data, 'dd');

  time = this.datePipe.transform(this.data, 'HH:mm');

  constructor(public navCtrl: NavController,
    private toast: ToastController,
    private firebaseAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private datePipe: DatePipe) {
    //データベースからデータ取得(keyを含める)
    this.ditems = this.db.list('Deper/')
    .snapshotChanges()
    .pipe(map(items => {
      return items.map(a => {
        const data = a.payload.val();
        const key = a.payload.key;
        return {key,  data};           // or {key, ...data} in case data is Obj
      });
    }));
  }

  //チェンジイベント
  public onChangeDeper(){
    //データベースからデータ取得(keyを含める)
    this.yitems = this.db.list('YearDeper/' + this.Deper)
    .snapshotChanges()
    .pipe(map(items => {
      return items.map(a => {
        const key = a.payload.key;
        return {key};           // or {key, ...data} in case data is Obj
      });
    }));    

  }

  //ログインに戻る
  public Loginsend(){
    this.navCtrl.setRoot(LoginPage);
  }

  //ログアウト
  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

  //新規作成ボタン
  public newaccount(user: User) {
    //アカウントの作成
    this.firebaseAuth
    .auth
    .createUserWithEmailAndPassword(user.email, user.password)
    .then(value => {
      //成功
      console.log('Success!', value);
      switch(this.Year){
        case "1": 
          this.Year = this.year;
          break;
        case "2": 
          this.Year = String(Number(this.year) - 1);
          break;
        case "3": 
          this.Year = String(Number(this.year) - 2);
          break;
      }
      //データをぶち込む(学籍番号と名前)
      this.db.object('Student/' + this.Deper + '/' + this.Year + '/' + value.user.uid).set({Number: this.number, Name: this.name});
      this.logout();
      this.toast.create({
        message: `アカウント作成`,
        duration: 3000
      }).present();
      this.navCtrl.setRoot(LoginPage);
    })
    .catch(err => {
      //失敗
      console.log('Something went wrong:',err.message);
      if(err.message == "The email address is badly formatted."){
        this.toast.create({
          message: `半角英数字で入力してください`,
          duration: 3000
        }).present();
      }
      if(err.message == "Password should be at least 6 characters"){
        this.toast.create({
          message: `パスワードは６文字以上です`,
          duration: 3000
        }).present();
      }

    });
    
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad NewaccountPage');
  }

}
