import { Component} from '@angular/core';
import { HomePage } from '../home/home';
import { NavParams } from 'ionic-angular'
import { LessonPage } from "../lesson/lesson";
import { RecordsPage } from "../records/records"
import { AnalyticsPage } from "../analytics/analytics";
import { LogoutPage } from '../logout/logout'
@Component({
  templateUrl: 'tabs.html'
})


export class TabsPage{

  homePage = HomePage;
  lessonPage = LessonPage;
  recordsPage = RecordsPage;
  analyticsPage = AnalyticsPage;
  logoutPage = LogoutPage;
  
  constructor(private navParams : NavParams) {
    
  }
  
  
}
