import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";

import { Observable } from 'rxjs/Observable';


import { DatePipe } from '@angular/common';

import 'rxjs/add/operator/first';
/**
 * Generated class for the TablePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-table',
  templateUrl: 'table.html',
})
export class TablePage {

  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //表
  tablelist: any[] = new Array;
  tableday: any[] = new Array;
  table: any[] = new Array;

  //受け取る変数
  //学科
  Deper: string;
  //学年
  Year: string;

  data = new Date();

  //年月  
  ym = this.datePipe.transform(this.data, 'y') + '-' + this.datePipe.transform(this.data, 'MM');

  //表示データ
  year: Number;
  DName: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private db: AngularFireDatabase,
    private datePipe: DatePipe) {

    //前ページから値受け取り
    this.Deper = navParams.get("Deper");
    this.Year = navParams.get("Year");
    this.year = navParams.get("year");
    this.DName = navParams.get("DName");

    //４月以降
    if(Number(this.datePipe.transform(this.ym, 'MM')) > 3){
      //データベースからデータ取得
      this.db.list('Student/' + this.Deper + '/' + this.Year)
      .snapshotChanges()
      .first()
      .subscribe(itemlist => {

        this.db.list('Record/' + itemlist[0].key + 
        '/' + this.datePipe.transform(this.ym, 'y') + 
        '/' + this.datePipe.transform(this.ym, 'MM'))
        .snapshotChanges()
        .first()
        .subscribe(day =>{

          for(var d = 0; d < day.length; d++){
          //日
          this.tableday.push(day[d].key);
          console.log(day[d].key);
          }
        });

        for(var i = 0; i < itemlist.length; i++){
          
          this.db.list('Student/' + this.Deper + '/' + this.Year + '/' + itemlist[i].key)
          .valueChanges()
          .first()
          .subscribe(name =>{

            //名前
            this.tablelist.push("【" + name[1] + "】" + name[0]);
    
          });

          this.db.list('Record/' + itemlist[i].key + 
          '/' + this.datePipe.transform(this.ym, 'y') + 
          '/' + this.datePipe.transform(this.ym, 'MM'))
          .valueChanges()
          .first()
          .subscribe(item =>{
            //if(itemlist.length > this.cnt){
              for(var n = 0; n < item.length; n++){
                //ステータス
                this.table.push(item[n]);
                console.log(item[n]);
              }
          });
        }
      });

    }else{//4月以前
      //データベースからデータ取得
      this.db.list('Student/' + this.Deper + '/' + this.Year)
      .snapshotChanges()
      .first()
      .subscribe(itemlist => {

        this.db.list('Record/' + itemlist[0].key + 
        '/' + String(Number(this.datePipe.transform(this.ym, 'y')) - 1) + 
        '/' + this.datePipe.transform(this.ym, 'MM'))
        .snapshotChanges()
        .first()
        .subscribe(day =>{

          for(var d = 0; d < day.length; d++){
          //日
          this.tableday.push(day[d].key);
          console.log(day[d].key);
          }
        });

        for(var i = 0; i < itemlist.length; i++){
          
          this.db.list('Student/' + this.Deper + '/' + this.Year + '/' + itemlist[i].key)
          .valueChanges()
          .first()
          .subscribe(name =>{

            //名前
            this.tablelist.push("【" + name[1] + "】" + name[0]);
    
          });

          this.db.list('Record/' + itemlist[i].key + 
          '/' + String(Number(this.datePipe.transform(this.ym, 'y')) - 1) + 
          '/' + this.datePipe.transform(this.ym, 'MM'))
          .valueChanges()
          .first()
          .subscribe(item =>{
            //if(itemlist.length > this.cnt){
              for(var n = 0; n < item.length; n++){
                //ステータス
                this.table.push(item[n]);
                console.log(item[n]);
              }
          });
        }
      });

    }

  }

  public YMChange(){

    //しょ・き・か♡
    this.tablelist = new Array;
    this.tableday = new Array;
    this.table = new Array;
  
    //４月以降
    if(Number(this.datePipe.transform(this.ym, 'MM')) > 3){
      //データベースからデータ取得
      this.db.list('Student/' + this.Deper + '/' + this.Year)
      .snapshotChanges()
      .first()
      .subscribe(itemlist => {

        this.db.list('Record/' + itemlist[0].key + 
        '/' + this.datePipe.transform(this.ym, 'y') + 
        '/' + this.datePipe.transform(this.ym, 'MM'))
        .snapshotChanges()
        .first()
        .subscribe(day =>{

          for(var d = 0; d < day.length; d++){
          //日
          this.tableday.push(day[d].key);
          console.log(day[d].key);
          }
        });

        for(var i = 0; i < itemlist.length; i++){
          
          this.db.list('Student/' + this.Deper + '/' + this.Year + '/' + itemlist[i].key)
          .valueChanges()
          .first()
          .subscribe(name =>{

            //名前
            this.tablelist.push("【" + name[1] + "】" + name[0]);
    
          });

          this.db.list('Record/' + itemlist[i].key + 
          '/' + this.datePipe.transform(this.ym, 'y') + 
          '/' + this.datePipe.transform(this.ym, 'MM'))
          .valueChanges()
          .first()
          .subscribe(item =>{
            //if(itemlist.length > this.cnt){
              for(var n = 0; n < item.length; n++){
                //ステータス
                this.table.push(item[n]);
                console.log(item[n]);
              }
          });
        }
      });

    }else{//4月以前
      //データベースからデータ取得
      this.db.list('Student/' + this.Deper + '/' + this.Year)
      .snapshotChanges()
      .first()
      .subscribe(itemlist => {

        this.db.list('Record/' + itemlist[0].key + 
        '/' + String(Number(this.datePipe.transform(this.ym, 'y')) - 1) + 
        '/' + this.datePipe.transform(this.ym, 'MM'))
        .snapshotChanges()
        .first()
        .subscribe(day =>{

          for(var d = 0; d < day.length; d++){
          //日
          this.tableday.push(day[d].key);
          console.log(day[d].key);
          }
        });

        for(var i = 0; i < itemlist.length; i++){
          
          this.db.list('Student/' + this.Deper + '/' + this.Year + '/' + itemlist[i].key)
          .valueChanges()
          .first()
          .subscribe(name =>{

            //名前
            this.tablelist.push("【" + name[1] + "】" + name[0]);
    
          });

          this.db.list('Record/' + itemlist[i].key + 
          '/' + String(Number(this.datePipe.transform(this.ym, 'y')) - 1) + 
          '/' + this.datePipe.transform(this.ym, 'MM'))
          .valueChanges()
          .first()
          .subscribe(item =>{
            //if(itemlist.length > this.cnt){
              for(var n = 0; n < item.length; n++){
                //ステータス
                this.table.push(item[n]);
                console.log(item[n]);
              }
          });
        }
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TablePage');
  }

}
