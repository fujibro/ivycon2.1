import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";

/**
 * Generated class for the DeperaddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-deperadd',
  templateUrl: 'deperadd.html',
})
export class DeperaddPage {

  //学科名
  name: string;

  //年
  year: string;

  constructor(private db:AngularFireDatabase, 
    private toast: ToastController) {
  }

  //学科追加
  public newdeper(){
    //学科追加
    const key = this.db.list('Deper/').push(this.name).key;

    //学年追加
    switch(this.year){
      case '1': this.db.object('YearDeper/'+key).set({1:1});  break;
      case '2': this.db.object('YearDeper/'+key).set({1:1, 2:2});  break;
      case '3': this.db.object('YearDeper/'+key).set({1:1, 2:2, 3:3});  break;
    }
    this.toast.create({
      message: `学科作成`,
      duration: 3000
    }).present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeperaddPage');
  }

}
