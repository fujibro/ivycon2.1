import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { PastattendPage } from '../pastattend/pastattend';
/**
 * Generated class for the PaststudentlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-paststudentlist',
  templateUrl: 'paststudentlist.html',
})
export class PaststudentlistPage {

  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //受け取る変数
  //学科
  Deper: string;
  //学年
  Year: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private db: AngularFireDatabase) {
      //前ページから値受け取り
      this.Deper = navParams.get("Deper");
      this.Year = navParams.get("Year");

      this.items = this.db.list('Student/' + this.Deper + '/' + this.Year)
      .snapshotChanges()
      .pipe(map(getdata => {
        return getdata.map(a => {
          //valueを取る
          const data = a.payload.val();
          //keyをとる
          const key = a.payload.key;
          //keyとvalue返す
          return {key, ...data};
        })
      }));
  }

  //クリックイベント
  public itemSelected(uid, name){

    this.navCtrl.push(PastattendPage,{
      //選択したUIDと名前を渡す
      UID: uid,
      Name: name,
      Year: this.Year
    });

  }
  
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad PaststudentlistPage');
  }

}
