import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { DatePipe } from '@angular/common';

/**
 * Generated class for the PastattendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pastattend',
  templateUrl: 'pastattend.html',
})
export class PastattendPage {
  //firebaseのデータの入れ子
  items: Observable<any[]>;

  //受け取る変数
  //UID
  UID: string;
  //生徒の名前
  Name: string;
  //年
  Year: string;

  //出席率
  Attend: any[] = new Array;

  //総日数
  sumday;

  //欠席
  ab;
  
  //遅刻早退
  late;

  dd: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private db: AngularFireDatabase, 
    private datePipe: DatePipe) {
      //前ページから値受け取り
      this.UID = navParams.get("UID");
      this.Name = navParams.get("Name");
      this.Year = navParams.get("Year") + '-01';

    //４月以降
    if(Number(this.datePipe.transform(this.Year, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.Year, 'y') + 
      '/' +  this.datePipe.transform(this.Year, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.Year, 'y'))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }else{//4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.Year, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.Year, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.Year, 'y')) - 1 ))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }
  }

  //年月の変更
  public YMChange(){
    //４月以降
    if(Number(this.datePipe.transform(this.Year, 'MM')) > 3){
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.Year, 'y') + 
      '/' +  this.datePipe.transform(this.Year, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  this.datePipe.transform(this.Year, 'y'))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }else{//4月以前
      //データベースからデータ取得(keyを含める)
      this.items = this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.Year, 'y')) - 1 ) + 
      '/' +  this.datePipe.transform(this.Year, 'MM'))
      .snapshotChanges()
      .pipe(map(items => {
        return items.map(a => {
          const data = a.payload.val();
          const key = a.payload.key;
          return {key, ...data};           // or {key, ...data} in case data is Obj
        });
      }));

      //出欠率の計算
      this.db.list('Record/' + this.UID + 
      '/' +  String(Number(this.datePipe.transform(this.Year, 'y')) - 1 ))
      .valueChanges()
      .subscribe(item => {
        //初期化
        //総日数
        this.sumday = 0;
        //欠席
        this.ab = 0;
        //遅刻早退
        this.late = 0;

        item.map(status => {
          //31回
          for(var day = 1; day < 31; day++){
            ////////datepipeはゴミ
            this.dd = day;
            if(day < 9){
              this.dd = '0' + day;
            }
            ///////
            //undifindチェック
            if(status[this.dd] != undefined){
              switch(status[this.dd].Status){
                case "出席":
                  this.sumday++;
                  break;
                case "欠席":
                  this.sumday++;
                  this.ab++;
                  break;
                case "遅刻":
                  this.sumday++;
                  this.late++;
                  break;
                case "早退":
                  this.sumday++;
                  this.late++;
                  break;
                case "公欠":
                  this.sumday++;
                  break;
              }
            }
          }
        })
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PastattendPage');
  }

}
